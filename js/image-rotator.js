/**
 * @file
 * Initialize the image rotator (jquery.cycle), if we have at least 2 images.
 *
 * Structure we are given is:
 *
 * <div class="image-rotator">
 *   <div id="image-rotator">
 *     <div class="rotator-item">
 *       <div class="rotator-item-image">...</div>
 *       <div class="rotator-item-caption">...</div>
 *     </div>
 *     ...
 *   </div>
 *   <div class="image-rotator-nav">
 *     <a href="#" id="image-rotator-prev">...</a>
 *     <a href="#" id="image-rotator-next">...</a>
 *   </div>
 *   <div id="image-rotator-pager">
 *     ... dynamically generated hyperlinks, 1 per rotator item ...
 *   </div>
 * </div>
 *
 *
 * Internet Explorer
 * =================
 * It looked like IE was failing because of something due to the Pager or Nav,
 * which lead to the following potential issue:
 * @see http://stackoverflow.com/questions/7762021/jquery-cycle-pageranchorbuilder-issue-with-ie8-and-lower
 *
 * However, it turns out that $(window).resize() events were being fired in IE
 * constantly, even when nothing occurred. This seems to be what was causing
 * the rotator to fail.
 *
 *
 * Positioning the Pager
 * =====================
 * We need to dynamically position the pager relative to the bottom of the
 * image.
 */

image_rotator = (typeof image_rotator === 'undefined') ? {} : image_rotator;
image_rotator.rotator = (function($) {
  // Define DOM elements, selectors and css classes.
  var _els = {
    // Elements are DOM element selectors that we inspect or manipulate.
    rotator:        '', // Gets defined in initVars().
    rotator_image:  '.rotator-item-image',

    rotator_item:   '.rotator-item',
    rotator_first:  '.rotator-item-first',

    btn_next:       '#image-rotator-next',
    btn_prev:       '#image-rotator-prev',
    pager:          '#image-rotator-pager'
  };

  var _slideshow = {
    has_nav:          false,  // Should we show next/previous buttons?
    has_pager:        false,  // Should we show page navigation?
    has_auto_advance: false   // Should we automatically advance to the next slide?
  };

  var _is_paused = false;

  // Track the browser width during window resize events, so we can validate
  // whether a resize event is legitimate or not. (Internet Explorer 8 under
  // VirtualBox -- and possibly not under VirtualBox) can fire these events
  // constantly, without any user interaction.
  var _last_width = -1;




  /**
   * At this time, Drupal.settings should have been populated. We can now
   * inspect it to get our configuration values.
   */
  var initVars = function() {
    _els.rotator = '#' + Drupal.settings.image_rotator.id;
    _slideshow.has_nav = Drupal.settings.image_rotator.has_nav;
    _slideshow.has_pager = Drupal.settings.image_rotator.has_pager;
    _slideshow.has_auto_advance = Drupal.settings.image_rotator.has_auto_advance;
  };


  
  /**
   * Initialize the cycler object on the group of rotator items.
   *
   * We're initially called after all images have loaded.
   *
   * We're also called after a browser resize event.
   */
  var initCycler = function(options) {
    if (typeof options === 'undefined') {
      options = getCycleOptions();
    }

    $(_els.rotator).cycle(options);

    // If the rotator has been configured to NOT auto-advance, ensure it doesn't.
    if (!_slideshow.has_auto_advance || _is_paused) {
      disableAutoAdvance();
    }

    if (_slideshow.has_pager) {
      positionPager();
    }
  };




  var activeSlide;

  /**
   * Define the rotator configuration options.
   */
  var getCycleOptions = function() {
    var options = {
      containerResize:  1,
      width:            'fit',
      slideResize:      1,

      after: function(curr, next, obj) {
        activeSlide = obj.currSlide;
      },

      // The following line (cleartypeNoBg) is required for Internet Explorer.
      // Without it, the transition doesn't occur, so it seems like the cycler
      // is broken.
      // @see: http://stackoverflow.com/questions/6228840/jquery-cycle-fade-transition-not-working-in-ie
      cleartypeNoBg:    true,
      fx:               'fade', // Default transition.

      speed:            'fast'

    };


    // If defined, add next/previous navigation elements.
    if (_slideshow.has_nav) {
      options.next = _els.btn_next;
      options.prev = _els.btn_prev;
    }

    // If defined, add pager navigation elements.
    if (_slideshow.has_pager) {
      options.pager = _els.pager;
    }


    return options;
  };


  /**
   * Disable the auto-advance functionality of the image rotator. We do this
   * when a user interacts with a component (prev/next buttons or the pager).
   */
  var disableAutoAdvance = function() {
    $(_els.rotator).cycle('pause');
    _is_paused = true;
  };



  /**
   * Position the pager element relative to the height of the first image in
   * the rotator group.
   *
   * We do this upon initial load, and after any resize event.
   */
  var positionPager = function() {
    // Get current height of the first image in the rotator.
    var el = '#js-rotator-item-' + activeSlide + ' .rotator-item-image';
    var height = $(el).height();

    if (height > 0) {
      /* 25 is an arbitrary value. It's the height of the pager plus some padding. */
      offset = height - 25; 
      $(_els.pager).css({
        'top': offset + 'px'
      });
    }
  };








  var public_interface = {};


  /**
   * Hide the images (except for the first one), because everything hasn't
   * finished loading yet. This avoids the user seeing the images stack on top
   * of each other and then disappearing into the rotator.
   */
  public_interface.prepForInit = function() {
    if ($(_els.rotator_image).length == 0) {
      return;
    }

    $(_els.rotator_item).hide();  // @todo this line causes IE7 to crash...? */
//    $(_els.rotator_item).addClass('hidden');
    $(_els.rotator_first).show();
  };



  /**
   * Start the jquery.cycle event if there are at least 2 images.
   *
   * We are called initially when all images are loaded.
   */
  public_interface.init = function() {
    if ($(_els.rotator_image).length == 0) {
      // This page doesn't have a rotator defined. (This condition shouldn't
      // occur because this javascript is only added by the code that formats
      // image rotator groups, but it's here just in case we change the js
      // aggregation model in the future.)
      return;
    }


    initVars();
    initCycler();


    // If the user clicks a previous/next navigation button, ensure we
    // disable any auto-advancing of the images.
    if (_slideshow.has_nav) {
      $(_els.btn_next).click(function() {
        disableAutoAdvance();
        return false;
      });

      $(_els.btn_prev).click(function() {
        disableAutoAdvance();
        return false;
      });
    }


    // If the user clicks one of the pager elements, disable any auto-
    // advancing of the images.
    if (_slideshow.has_pager) {
      $(_els.pager + ' a').click(function() {
        disableAutoAdvance();
        return false;
      });
    }


    // In some browsers (usually Internet Explorer), the rotator won't
    // appear upon initial launch and requires a resize event. This is
    // related to responsive css, but not always. Force a fake window
    // resize so Internet Explorer displays the image.
    _last_width = -2;
    $('window').trigger('resize');

  };




  // Make the image rotator gallery responsive, when the browser window resizes.
  // @see: http://stackoverflow.com/questions/7003290/making-a-jquery-cycle-plugin-become-responsive-to-the-layout-pixels
  //
  // For some reason, IE8 (in VirtualBox) receives continual 'window resize'
  // events, even when nothing is happening. Therefore, we keep track of the
  // window size the last time we did anything, to ensure we're only updating
  // when necessary.
  $(window).resize(function() {
    var browser_width = $('body').width();
    if (browser_width == _last_width) {
      // This wasn't a legitimate browser resize event. Ignore it.
      return;
    }
    _last_width = browser_width;

    $(_els.rotator).cycle('destroy');
    $(_els.rotator).each(function() {
      new_width = $(this).parent('div').width();
      $(this).width(new_width);
      $(this).height('auto');
      $(this).children('div').width(new_width);
      $(this).children('div').height('auto');
    });

    var options = getCycleOptions();
    options.startingSlide = activeSlide;

    initCycler(options);
  });


  return public_interface;

})(jQuery);



(function ($) {
  // Before all the images are loaded, hide them (except for the first one).
  // This avoids seeing them stack up, and then disappear into the rotator.
  $(document).ready(function() {
    image_rotator.rotator.prepForInit();
  });

  // Now that all images are loaded, initialize the rotator.
  // @see: http://productforums.google.com/forum/#!topic/chrome/V-Qd5B6GSLA
  $(window).load(function() {
    image_rotator.rotator.init(); 
  });
})(jQuery); 


